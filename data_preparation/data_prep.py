#!/bin/python
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
import numpy as np
import pandas as pd
import time
import matplotlib.ticker as ticker
import matplotlib
matplotlib.use('pdf')
import matplotlib.pyplot as plt
import os
from commons import *

dirpath=os.environ['name3']
name=os.environ['name2']
input=os.environ['input_txt']

uniform_sample=Uniform(dirpath, name, input)

flip_sample=FlipAngle(dirpath, name, uniform_sample)

norm_sample=MeanStdCreator(dirpath, name, flip_sample)

print "Final sample saved at:   ", norm_sample