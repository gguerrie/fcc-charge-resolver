#!/bin/bash

#read -r -p "Welcome in the data preparation script; please insert the path to the folder containing the ntuples names: " name1
#echo ""
#read -r -p "Insert the name of this training sample: " name2
#read -r -p "Insert the path for the output files: " name3

# Set the color variable
green='\033[0;32m'
red='\e[1;31m'
# Clear the color after that
clear='\033[0m'

if [ -z ${1} ]
then
    config=./config.ini
    echo "No config file, setting ./config.ini"
else
    export config=$1
fi

echo ""
echo "===================================================================="
name1=$(sed -nr "/^\[DATA\]/ { :l /^input_folder[ ]*=/ { s/.*=[ ]*//; p; q;}; n; b l;}" ./$config)
export name1=${name1//[[:blank:]]/}
echo -e -n "Input folder path:  ${red}$name1${clear}";


echo ""
echo "===================================================================="
name2=$(sed -nr "/^\[DATA\]/ { :l /^sample_name[ ]*=/ { s/.*=[ ]*//; p; q;}; n; b l;}" ./$config)
export name2=${name2//[[:blank:]]/}
echo -e -n "Name for the sample:  ${red}$name2${clear}";

echo ""
echo "===================================================================="
name3=$(sed -nr "/^\[DATA\]/ { :l /^output_folder[ ]*=/ { s/.*=[ ]*//; p; q;}; n; b l;}" ./$config)
export name3=${name3//[[:blank:]]/}
echo -e -n "Output folder path:  ${red}$name3${clear}";
echo ""
echo ""




if [ -d "$name3/training/data" ] && [ -d "$name3/training/preprocessing" ]; then
  ### Take action if log exists ###
  echo "INFO: Folder structure ok"
else
  ###  Control will jump here if $DIR does NOT exists ###
  mkdir -p $name3/training/data
  mkdir -p $name3/training/preprocessing
  echo "INFO: Folders ok"
fi

# Prepare the filtext containing the list of .root files to analyze
ls -1 $name1/* > training_ntuples/training_ntuples_${name2}.txt

echo "INFO: The file list can be found in $PWD/training_ntuples/training_ntuples_${name2}.txt"

# Convert the root files in .csv python-readable files
source ../setup_lxplus.sh

export input=$PWD/training_ntuples/training_ntuples_${name2}.txt
export output=$name3/training/preprocessing/training_${name2}.txt

root -l ../0_ntuple_to_txt/NtupToTxt.C

#source /gpfs/atlas/gguerrie/DNN/setup_python.sh
export name2=$name2
export name3=$name3/training/preprocessing
export input_txt=$output

python data_prep.py

