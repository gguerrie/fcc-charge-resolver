#!/bin/python
# normalizzo dati e label

import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.models import Model
from tensorflow.keras.layers import Dense, Input, Conv2D, Dropout, Flatten, Concatenate, Reshape, BatchNormalization
from tensorflow.keras.callbacks import EarlyStopping, ReduceLROnPlateau, TerminateOnNaN
import numpy as np
import pandas as pd
import time
import matplotlib.ticker as ticker
#from keras import keras.utils
#from keras.utils import plot_model
import matplotlib
matplotlib.use('pdf')
import matplotlib.pyplot as plt
import os

if "jobName" in os.environ:
    name=os.environ.get("jobName")


def getDataLabels(data):
    data.fillna(0, inplace = True)
    labels = data.pop(0)
    return data, labels

def norm(data, mean, std):
    normed_data = (data - mean)/std
    return normed_data

def snorm(normed_data, mean, std):
    snorm = normed_data*std + mean
    return snorm

path = '/eos/infnts/atlas/gguerrie/DNN/training/'

raw_train_data = pd.read_csv(path + 'training_mca_dil_norm.csv', sep=",", header=None)
normed_train_data, normed_train_labels = getDataLabels(raw_train_data)

title = 'size_' + str(len(normed_train_data))   

inputLayer = Input(shape=(25,))
x = BatchNormalization()(inputLayer)
####
x = Dense(100, activation='relu')(x)
#x = Dropout(rate=0.10)(x)
####
x = Dense(80, activation='relu')(x)
#x = Dropout(rate=0.10)(x)
####
x = Dense(60, activation='relu')(x)
#x = Dropout(rate=0.10)(x)
####
outputLayer = Dense(1, activation='linear')(x)
####
model = Model(inputs=inputLayer, outputs=outputLayer)

model.compile(optimizer = tf.keras.optimizers.Adam(lr=0.0001),                     
             loss = 'mae',
             metrics = ['mse'])
model.summary()

start = time.time()
print("Start training")
history = model.fit(normed_train_data, normed_train_labels, 
                    epochs = 100, validation_split = 0.2,                                            
                    batch_size = 50, verbose = 2, 
                    callbacks = [
                    EarlyStopping(monitor='val_loss', patience=10, verbose=1),
                    ReduceLROnPlateau(monitor='val_loss', factor=0.1, patience=2, verbose=1),
                    TerminateOnNaN()]) 
end = time.time()
print('Time spent on training: ' + str(int((end - start)/60)) + ' minutes' ) 

model.save(path + 'models/' + title + "_" + name + "_2022.h5")

hist_df = pd.DataFrame(history.history) 
hist_df.insert(loc=0, column='epoch', value=hist_df.index + 1)
hist_df.to_csv(path + 'history/' + title + '.csv', index=False, header=True)

fig = plt.figure(figsize=(8,4))

# loss
plt.subplot(121)
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('Loss: Mean Absolute Error')
plt.ylabel('mae')
plt.xlabel('epoch')
plt.legend(['train', 'val'], loc='upper right')
axes = plt.gca()
axes.set_ylim([0.34,0.42]) 

# metrics
plt.subplot(122)
plt.plot(history.history['mse'])
plt.plot(history.history['val_mse'])
plt.title('Metrics: Mean Squared Error')
plt.ylabel('mse')
plt.xlabel('epoch')
plt.legend(['train', 'val'], loc='upper right')
axes = plt.gca()
axes.set_ylim([0.24,0.40])
plt.tight_layout()
fig.savefig(path + 'images/' + title + "_" + name + '_training.png')

print(title + "_" + name + "_2022.h5")
