#!bin/sh

# setupATLAS
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh --quiet

# check this version!
ARCH="x86_64-centos7-gcc8-opt"
#http://lcginfo.cern.ch/

# LCG="LCG_96b"
LCG="LCG_98"

lsetup "root 6.18.04-${ARCH}"

