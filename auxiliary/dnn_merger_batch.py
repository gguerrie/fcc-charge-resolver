import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
import tensorflow as tf
from tensorflow import keras
import numpy as np
from numpy import genfromtxt
import time
import sys
import os
import ROOT
from array import array

start_time = time.time()

infile = sys.argv[1]
outfile = sys.argv[2]

#Import the NN model
#model = tf.keras.models.load_model('/gpfs/atlas/gguerrie/uniform/models/size_234282.h5', compile = False)
model = tf.keras.models.load_model('/gpfs/atlas/gguerrie/uniform/models/size_234282.h5', compile = True)
#Import the TFile to read
print("---------------------------------")
print("Opening TFile: " + infile)
f = ROOT.TFile.Open(infile)
#f = ROOT.TFile.Open("/eos/infnts/atlas/public/ttRes_L2ntuples/V23-nominal/emu_ttbar_dilep/ttbar_dil_mca.root")
myTree = f.Get("nominal")
#Create new TFile onto which write the new mttbar branch
newfile = ROOT.TFile(outfile,"RECREATE")
newtree = myTree.CloneTree(0);
leafValues = array('d',[0])
newBranch = newtree.Branch( "dnn_mttbar" , leafValues, 'uniform/D')

#Initialize the branches used as input for the NN  
branches = ["el_pt[0]","el_eta[0]","el_phi[0]","el_e[0]","mu_pt[0]","mu_eta[0]","mu_phi[0]","mu_e[0]","jet_pt[0]","jet_eta[0]","jet_phi[0]","jet_e[0]","jet_pt[1]","jet_eta[1]","jet_phi[1]","jet_e[1]","jet_pt[2]","jet_eta[2]","jet_phi[2]","jet_e[2]","met_met","met_phi"]

el_pt = ROOT.std.vector("float")(1)
myTree.SetBranchAddress("el_pt",el_pt);

el_eta = ROOT.std.vector("float")(1) 
myTree.SetBranchAddress("el_eta",el_eta);

el_phi = ROOT.std.vector("float")(1)
myTree.SetBranchAddress("el_phi",el_phi);

el_e = ROOT.std.vector("float")(1)
myTree.SetBranchAddress("el_e",el_e);

mu_pt = ROOT.std.vector("float")(1)
myTree.SetBranchAddress("mu_pt",mu_pt);

mu_eta = ROOT.std.vector("float")(1)
myTree.SetBranchAddress("mu_eta",mu_eta);

mu_phi = ROOT.std.vector("float")(1)
myTree.SetBranchAddress("mu_phi",mu_phi);

mu_e = ROOT.std.vector("float")(1)
myTree.SetBranchAddress("mu_e",mu_e);

jet_pt = ROOT.std.vector("float")(3)
myTree.SetBranchAddress("jet_pt",jet_pt);

jet_eta = ROOT.std.vector("float")(3)
myTree.SetBranchAddress("jet_eta",jet_eta);

jet_phi = ROOT.std.vector("float")(3)
myTree.SetBranchAddress("jet_phi",jet_phi);

jet_e = ROOT.std.vector("float")(3)
myTree.SetBranchAddress("jet_e",jet_e);

met_met = 0
myTree.SetBranchAddress("met_met",met_met);
met_phi = 0
myTree.SetBranchAddress("met_phi",met_phi);

#Define number of events to run on
#Take min between Max and myTree.GetEntriesFast()
maxEvents = 1000000
if myTree.GetEntriesFast() < maxEvents:
    maxEvents = myTree.GetEntriesFast()

#Define the subset of entries to be processed in each iteration
batch_size = 10000
iter=range(0, maxEvents, batch_size)
iter+=[int(maxEvents)]

#Initialize variables that will enclose the inputs and the predictions
matrix = np.zeros((batch_size, len(branches)))
mean = np.zeros((1, len(branches)+1))
std = np.zeros((1, len(branches)+1))
uniform_predictions = np.zeros(batch_size)

#Import the mean and std files obtained from training  (WARNING: shape(23), have to assign the first row to a different label)
mean[0,:]=genfromtxt('/gpfs/atlas/gguerrie/uniform/mean_normed_3TeV_234282.txt', delimiter=',')
label_mean=mean[0,0]
mean = np.delete(mean, 0)
std[0,:]=genfromtxt('/gpfs/atlas/gguerrie/uniform/std_normed_3TeV_234282.txt', delimiter=',')
label_std=std[0,0]
std = np.delete(std, 0)

print("Load tree, get model, load mean and std")
print("--- %s seconds ---" % (time.time() - start_time))

#Enter in the loop
print("Total number of events: " + str(maxEvents))
print("Batch size: " + str(batch_size))
#print('Batch list: ', end='')
sys.stdout.write("Batch list: ")
sys.stdout.flush()
print(iter)
#print("Analysing event #: ")

for s in iter :
  # skip first
  if s == 0:
      continue
  # print the range considered in this batch
  print(" --> Range " + str(iter[iter.index(s)]) + " - " + str(iter[iter.index(s)-1]))
  # re-initialize matrix
  #print("Init matrix")
  matrix = np.zeros((iter[iter.index(s)] - iter[iter.index(s)-1], len(branches)))
  
  # loop on all entries in range
  #print("Looping on entries")
  for entry in range(iter[iter.index(s)-1], iter[iter.index(s)]):
    #print("  get entries")
    myTree.GetEntry(entry)
    #print("  looping on branches")
    
    matrix[entry - iter[iter.index(s)-1],0] = myTree.el_pt[0]
    matrix[entry - iter[iter.index(s)-1],1] = myTree.el_eta[0]
    matrix[entry - iter[iter.index(s)-1],2] = myTree.el_phi[0]
    matrix[entry - iter[iter.index(s)-1],3] = myTree.el_e[0]
    
    matrix[entry - iter[iter.index(s)-1],4] = myTree.mu_pt[0]
    matrix[entry - iter[iter.index(s)-1],5] = myTree.mu_eta[0]
    matrix[entry - iter[iter.index(s)-1],6] = myTree.mu_phi[0]
    matrix[entry - iter[iter.index(s)-1],7] = myTree.mu_e[0]
    
    matrix[entry - iter[iter.index(s)-1],8] = myTree.jet_pt[0]
    matrix[entry - iter[iter.index(s)-1],9] = myTree.jet_eta[0]
    matrix[entry - iter[iter.index(s)-1],10] = myTree.jet_phi[0]
    matrix[entry - iter[iter.index(s)-1],11] = myTree.jet_e[0]
    
    if myTree.jet_pt.size()==2:
      matrix[entry - iter[iter.index(s)-1],12] = myTree.jet_pt[1]
      matrix[entry - iter[iter.index(s)-1],13] = myTree.jet_eta[1]
      matrix[entry - iter[iter.index(s)-1],14] = myTree.jet_phi[1]
      matrix[entry - iter[iter.index(s)-1],15] = myTree.jet_e[1]
      
      matrix[entry - iter[iter.index(s)-1],16] = 0
      matrix[entry - iter[iter.index(s)-1],17] = 0
      matrix[entry - iter[iter.index(s)-1],18] = 0
      matrix[entry - iter[iter.index(s)-1],19] = 0
      
    if myTree.jet_pt.size()>2:
      matrix[entry - iter[iter.index(s)-1],12] = myTree.jet_pt[1]
      matrix[entry - iter[iter.index(s)-1],13] = myTree.jet_eta[1]
      matrix[entry - iter[iter.index(s)-1],14] = myTree.jet_phi[1]
      matrix[entry - iter[iter.index(s)-1],15] = myTree.jet_e[1]
      
      matrix[entry - iter[iter.index(s)-1],16] = myTree.jet_pt[2]
      matrix[entry - iter[iter.index(s)-1],17] = myTree.jet_eta[2]
      matrix[entry - iter[iter.index(s)-1],18] = myTree.jet_phi[2]
      matrix[entry - iter[iter.index(s)-1],19] = myTree.jet_e[2]
    
    matrix[entry - iter[iter.index(s)-1],20] = myTree.met_met
    matrix[entry - iter[iter.index(s)-1],21] = myTree.met_phi

        
  # call model.predict on full batch
  #print("Marix transformation")
  matrix = (matrix - mean) / std
  #print("Before predict...")
  #uniform_predictions = model.predict_on_batch(matrix).flatten()
  uniform_predictions = model.predict_on_batch(matrix)
  #uniform_predictions = model.predict(matrix)
  #print("After predict...")
  uniform_predictions = uniform_predictions*label_std + label_mean
  #print("Evaluate on model, entry # %s" % s)
  #print("--- %s seconds ---" % (time.time() - start_time))
  
  # re-loop over the events in the range, and fill the tree with the DNN output
  for entry in range(iter[iter.index(s)-1], iter[iter.index(s)]):
    myTree.GetEntry(entry)
    leafValues[0] = uniform_predictions[entry - iter[iter.index(s)-1]]
    newtree.Fill()
    
  print("Written on tree, entry # %s" % s)
  print("--- %s seconds ---" % (time.time() - start_time))
  print("=================================")
    

print("Finished.")
print("--- %s seconds ---" % (time.time() - start_time))

print("Writing new TFile...")  
newfile.Write()
newfile.Close()
