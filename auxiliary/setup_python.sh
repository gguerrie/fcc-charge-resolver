#!bin/sh

# setupATLAS
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh --quiet

# check this version!
ARCH="x86_64-centos7-gcc8-opt"
#http://lcginfo.cern.ch/

# LCG="LCG_96b"
LCG="LCG_98"

lsetup "root 6.18.04-${ARCH}"
lsetup "lcgenv -p ${LCG} ${ARCH} pyanalysis"
lsetup "lcgenv -p ${LCG} ${ARCH} pytools"
lsetup "lcgenv -p ${LCG} ${ARCH} keras"
lsetup "lcgenv -p ${LCG} ${ARCH} tensorflow"
lsetup "lcgenv -p ${LCG} ${ARCH} keras_applications"
lsetup "lcgenv -p ${LCG} ${ARCH} keras_preprocessing"

export OMP_NUM_THREADS=1

alias py=python
